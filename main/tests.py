from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("main:activity")
        self.test_activity = Activity.objects.create(
            activity = "boci"
        )
        self.test_member = Member.objects.create(
            member = "fikri",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("main:activityDelete",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("main:memberDelete",args=[self.test_member.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "main/index.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "tidur"
        }, follow=True)
        self.assertContains(response, "tidur")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "hi",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "hi")

    def test_notValid_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "hi",
            "aktivitas" : "huhu",
        }, follow=True)
        self.assertContains(response, "Input tidak sesuai")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        # print(response.content)
        self.assertContains(response, "berhasil dihapus")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "berhasil dihapus")
