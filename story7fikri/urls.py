from django.urls import path

from . import views

app_name = 'story7fikri'

urlpatterns = [
    path('', views.home, name='home'),
]
