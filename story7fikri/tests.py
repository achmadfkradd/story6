from django.test import TestCase, Client
from django.urls import reverse, resolve

class TestViews(TestCase):
    def test_GET(self):
        response = Client().get(reverse("story7fikri:home"))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story7.html")
