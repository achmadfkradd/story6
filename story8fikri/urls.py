from django.urls import path

from . import views

app_name = 'story8fikri'

urlpatterns = [
    path('', views.book, name='book'),
    path('volumes/', views.search, name='search')
]
