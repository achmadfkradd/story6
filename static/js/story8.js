function addDataToTable(tbody, i, item) {
    var info = item.volumeInfo;
    var publisher = $('<p>').text("Publisher: ");
    publisher.append($('<nobr>').text(info.publisher ? info.publisher : '-'));
    var authors = info.authors ? info.authors : '-';
    var cover = info.imageLinks ? $('<img>').attr({
        'src': info.imageLinks.smallThumbnail.replace("http://", "https://")
    }) : 'Info';
    var link = $('<a>').attr({
        'href': info.previewLink
    }).append(cover);
    var th_no = $('<td>').attr({
        'scope': 'row'
    }).text(i + 1);
    var authors1 = $('<p>').text("Authors: ");
    if (authors != '-') {
        $.each(authors, function (i, item) {
            authors1.append($('<nobr>').text(item));
        });
    } else {
        authors1.append($('<nobr>').text(authors));
    }
    var pages = $('<p>').text("Pages: ");
    pages.append($('<nobr>').text(info.pageCount ? info.pageCount : '-'));
    var date = $('<p>').text("Date: ");
    date.append($('<nobr>').text(info.publishedDate));
    var td_cover = $('<td>').append(link);
    var title = $('<p>').text("Title: ");
    title.append($('<nobr>').text(info.title));
    var subTitle = $('<p>').text("Subtitle: ");
    subTitle.append($('<nobr>').text(info.subtitle));
    var td_information = $('<td>').append(title, subTitle, authors1, publisher, date, pages);
    var $tr = $('<tr>').append(
        th_no,
        td_information,
        td_cover,
    ).appendTo(tbody);
}

function writeResults(items) {
    var val = document.getElementById("searchfield").value;
    $('.keyword-results').show();
    $('#book-results-table').show();
    var tbody = $('#results');
    tbody.empty();
    $(function () {
        $.each(items, function (i, item) {
            addDataToTable(tbody, i, item);
        });
    });
    $('#results').show();
}

$("#submit").click(function () {

    var getDataInformation = {
        'q': $('input[name=searchfield]').val(),
        'maxResults': 10,
    };

    $.ajax({
            type: 'GET',
            url: 'volumes?' + $.param(getDataInformation),
            dataType: 'json',
        })

        .done(function (data) {
            writeResults(data.items);
        });
    event.preventDefault();
});
