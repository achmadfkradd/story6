$( function() {
    $( "#accordion" )
      .accordion({
        header: "> div > #head",
        collapsible: true,
        icons:false,
        heightStyle: "content",
        active: 0
      })
  } 
  );

  $( document ).ready(function() {
    setButtons();
    $(document).on('click', '.sort-down', function(e) {
    var cCard = $(this).closest('.group');
    var tCard = cCard.next('.group');
    cCard.insertAfter(tCard);
    setButtons();
    });
    
    $(document).on('click', '.sort-up', function(e) {
    var cCard = $(this).closest('.group');
    var tCard = cCard.prev('.group');
    cCard.insertBefore(tCard);
    setButtons();
    });
    
    function setButtons(){
        $('button').show();
        $('.group:first-child  button.sort-up').hide();
        $('.group:last-child  button.sort-down').hide();
    }
    
    });
