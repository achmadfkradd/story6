{% load static %}
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Login Page</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    />
    <link rel="stylesheet" href="{% static 'css/story8.css' %}" />
    <script
      type="text/Javascript"
      src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"
    ></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>
  {% block content %}
  <div class="SignIn">
    <div class="container h-100">
      <div class="d-flex justify-content-center h-100">
        <div class="user_card">
          <div class="d-flex justify-content-center">
            <div class="brand_logo_container">
              <img
                src="{% static 'assets/hoi.jpg' %}"
                class="brand_logo"
                alt="Logo"
              />
            </div>
          </div>
          <div class="d-flex justify-content-center form_container">
            <form method="POST">
              {% csrf_token %}
              <div class="input-group mb-3">
                <input
                  type="text"
                  name="username"
                  class="form-control input_user"
                  value=""
                  placeholder="username"
                />
              </div>
              <div class="input-group mb-2">
                <input
                  type="password"
                  name="password"
                  class="form-control input_pass"
                  value=""
                  placeholder="password"
                />
              </div>
              <div class="d-flex justify-content-center mt-3 login_container">
                <button type="submit" name="button" class="btn login_btn">
                  Sign In
                </button>
              </div>
            </form>
          </div>
          {% if error %}
          <div class="alert alert-warning alert-dismissible fade show">
            <strong>Warning!</strong> {{ error }}
          </div>
          {% endif %}
        </div>
      </div>
    </div>
  </div>

  {% endblock content %}
</html>
